import './App.css';
import MultipleCounter from './store/features/counter/MultipleCounter';

function App() {
  return (
    <div className='App'>
      <MultipleCounter />
    </div>
  );
}

export default App;
