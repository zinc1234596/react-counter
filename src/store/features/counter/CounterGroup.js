import { useSelector } from 'react-redux';
import Counter from './Counter';

const CounterGroup = () => {
  const counterList = useSelector(state => state.counter.counterList);

  return (
    <div>
      {counterList.map((counter, index) => {
        return (
          <Counter
            counter={counter}
            key={index}
            index={index}
          />
        );
      })}
    </div>
  );
};

export default CounterGroup;
