import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: []
    },
    reducers: {
        changeCounterListSize: (state, action) => {
            state.counterList = action.payload ? Array.from({length:action.payload}).fill(0):[];
        },
        increase(state, action) {
            state.counterList[action.payload]++;
        },
        decrease(state, action) {
            state.counterList[action.payload]--;
        }
    }
})

export const { changeCounterListSize, increase, decrease } = counterSlice.actions

export default counterSlice.reducer;

