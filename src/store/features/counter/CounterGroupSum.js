import { useSelector } from "react-redux";

const CounterGroupSum = () => {
  const sum = useSelector(state => {
    return state.counter.counterList.reduce((sum, currentValue) => {
      return sum + currentValue;
    }, 0)
  });
  return <>Sum:{sum}</>;
};

export default CounterGroupSum;
