import { useDispatch, useSelector } from "react-redux";
import { changeCounterListSize } from "./counterSlice";

const CounterSizeGenerator = (props) => {
  const counterList = useSelector(state => state.counter.counterList);
  const dispatch = useDispatch();

  const updateSize = (event) => {
    dispatch(changeCounterListSize(event.target.value));
  };
  
  return (
    <div>
      Size: <input type='number' value={counterList.length || ''} onChange={updateSize} />
    </div>
  );
};

export default CounterSizeGenerator;
