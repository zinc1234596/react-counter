import { useDispatch } from "react-redux";
import { increase, decrease } from "./counterSlice";

const Counter = (props) => {
  const { index, counter } = props
  const dispatch = useDispatch();

  return (
    <div className='counter'>
      <button onClick={() => { dispatch(increase(index)) }}>+</button>
      <h3>{counter}</h3>
      <button onClick={() => { dispatch(decrease(index)) }}>-</button>
    </div>
  );
};

export default Counter;
