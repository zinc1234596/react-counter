import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Counter from '../store/features/counter/Counter';

test('should show counter number', () => {
  render(<Counter counter={8} updateCounterValue={jest.fn()} />);
  expect(screen.getByText('8')).toBeVisible();
});

test('should increase the count when click on +', () => {
  const fakeUpdateCounterValue = jest.fn();
  render(<Counter counter={8} updateCounterValue={fakeUpdateCounterValue} />);
  userEvent.click(screen.getByText('+'));
  expect(fakeUpdateCounterValue).toBeCalledWith(9);
});
